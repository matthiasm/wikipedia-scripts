#!/usr/bin/python3

import pywikibot
import re
import requests

S = requests.Session()

URL = "https://en.wikipedia.org/w/api.php"

PARAMS = {
    "action": "query",
    "format": "json",
    "list": "exturlusage",
    "euquery": "atariage.com"
}

R = S.get(url=URL, params=PARAMS)
DATA = R.json()

EXTURLS = DATA["query"]["exturlusage"]

site = pywikibot.Site('en', 'wikipedia')

pattern = r"\[https?://www\.atariage\.com/software_page.html\?SoftwareLabelID=(\d+).*"
replacement = r"{{AtariAge software|id=\1}}"

exturls = []
skipped = []

while True:
    R = S.get(url=URL, params=PARAMS)
    DATA = R.json()

    exturls.extend(DATA["query"]["exturlusage"])

    if 'continue' not in DATA:
        break

    PARAMS.update(DATA['continue'])

    for exturl in exturls:

        title = exturl["title"]
        if title in skipped:
            continue

        if str(title).startswith('Wikipedia'):
            continue
        if str(title).startswith('Talk'):
            continue
        if str(title).startswith('User'):
            continue
        if str(title).startswith('File'):
            continue

        print(title)

        page = pywikibot.Page(site, title)
        text = page.text
        new_text = re.sub(pattern, replacement, text)
        if new_text != text:
            pywikibot.showDiff(text, new_text)
            user_input = input("Save page? (y/n): ")
            if user_input.lower() == 'y':
                page.text = new_text
                page.save(summary='[[Template:AtariAge software]]', minor=False)
            else:
                skipped.append(title)
        else:
            skipped.append(title)
        print()
